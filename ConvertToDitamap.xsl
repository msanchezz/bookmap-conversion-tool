<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">

    <xsl:output indent="yes" method="xml" encoding="utf-8" omit-xml-declaration="no"/>
    <xsl:strip-space elements="*"/>
 
    <!-- Start off with identity transform pattern. -->
    <!-- Copy everything from root level and convert as needed -->
    <xsl:template match="/ | @* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="bookmap">
        <xsl:text disable-output-escaping="yes">
&lt;!DOCTYPE map PUBLIC "-//OASIS//DTD DITA Map//EN" "map.dtd"&gt;
        </xsl:text>
        <map>
            <title>
                <xsl:value-of select="booktitle/mainbooktitle"/>
            </title>
            <topicref navtitle="{booktitle/mainbooktitle}">
                <xsl:apply-templates/>    
            </topicref>
        </map>
    </xsl:template>


    <xsl:template match="chapter">
        <topicref>
            <xsl:if test="@href !=''">
                <xsl:attribute name="href" select="replace(@href, ' ', '%20')"/>
            </xsl:if>
            <xsl:if test="@navtitle !=''">
                <xsl:attribute name="navtitle" select="@navtitle"/>
            </xsl:if>
            <xsl:if test="@format !=''">
                <xsl:attribute name="format" select="@format"/>
            </xsl:if>
            <xsl:apply-templates></xsl:apply-templates>
        </topicref>
    </xsl:template>


    <xsl:template match="topicref">
        <topicref>
            <xsl:if test="@href !=''">
                <xsl:attribute name="href" select="replace(@href, ' ', '%20')"/>
            </xsl:if>
            <xsl:if test="@navtitle !=''">
                <xsl:attribute name="navtitle" select="@navtitle"/>
            </xsl:if>
            <xsl:if test="@format !=''">
                <xsl:attribute name="format" select="@format"/>
            </xsl:if>
            <xsl:apply-templates></xsl:apply-templates>
        </topicref>
    </xsl:template>


    <xsl:template match="topichead">
        <topichead>
            <xsl:if test="@href !=''">
                <xsl:attribute name="href" select="replace(@href, ' ', '%20')"/>
            </xsl:if>
            <xsl:if test="@navtitle !=''">
                <xsl:attribute name="navtitle" select="@navtitle"/>
            </xsl:if>
            <xsl:if test="@format !=''">
                <xsl:attribute name="format" select="@format"/>
            </xsl:if>
            <xsl:apply-templates></xsl:apply-templates>
        </topichead>
    </xsl:template>


    <!-- Drop stuff that doesn't fit into basic DITA Map context -->
    <xsl:template match="backmatter"></xsl:template>
    <xsl:template match="booktitle"></xsl:template>
    <xsl:template match="mainbooktitle"></xsl:template>
    <xsl:template match="bookmeta"></xsl:template>
    <xsl:template match="frontmatter"></xsl:template>
    <xsl:template match="@class"></xsl:template>


</xsl:stylesheet>